<?php include 'inc/navbar.php' ?>

<br>
<div class="container">
  <a class="btn waves-effect waves-light green accent-3 left" href="page-admin.php"><i class="material-icons right">undo</i>back</a>
</div>
<br>
<div class="container z-depth-2">
  <h4 class="center">ABSENSI SISWA TIDAK HADIR X MM 1</h4>
  <p class="divider"></p>
  <div class="container">
        <table class="striped centered responsive-table">
          <thead class="green accent-3 white-text">
            <tr>
              <th>No.</th>
              <th data-field="nama">NAMA</th>
              <th data-field="date">Tanggal</th>
              <th data-field="keterangan">Keterangan</th>
              <th data-field="alasan">Alasan</th>
              <th data-field="opsi">Opsi</th>
            </tr>
          </thead>

         <?php 
  include('koneksi/koneksi.php');
  $query = mysql_query("select * from table_xmm1");
  
  $no = 1;
  while ($data = mysql_fetch_array($query)) {
  ?>
      <tr>
          <td class="center aligned"><?php echo $no; ?></td>
          <td class="center aligned"><?php echo $data['nama']; ?></td>
          <td class="center aligned"><?php echo $data['date']; ?></td>
          <td class="center aligned"><?php echo $data['keterangan']; ?></td>
          <td class="center aligned"><?php echo $data['alasan']; ?></td>
          <td>
            <a href="edit_guru.php?id=<?php echo $data['id']; ?> "onclick="return confirm ('Anda Yakin Ingin Mengubh Record ini? ?')" class="btn-floating waves-effect waves-light black"><i class="material-icons">mode_edit</i></a>
            <a href="delete-siswa-xmm1.php?id=<?php echo $data['id']; ?>" onclick="return confirm ('Anda Yakin Ingin Menghapus Record ini?')" class="btn-floating waves-effect waves-light black">
            <i class="material-icons">delete</i>
            </a>
          </td>
        </tr>
    <?php 
    $no++;
  } 
  ?>
        </table>
    </div>
    <br>
</div>

<div class="container z-depth-2">
  <h4 class="center">ABSENSI GURU TIDAK HADIR X MM 1</h4>
  <p class="divider"></p>
  <div class="container">
        <table class="striped centered responsive-table">
          <thead class="green accent-3 white-text">
            <tr>
              <th>No.</th>
              <th data-field="jam_ke">jam ke</th>
              <th data-field="nama_guru">NAMA GURU & MATA DIKLAT</th>
              <th data-field="alasan">Alasan</th>
              <th data-field="penugasan">penugasan</th>
              <th data-field="jenis_tugas">jenis_tugas</th>
              <th data-field="opsi">Opsi</th>
            </tr>
          </thead>

         <?php 
  include('koneksi/koneksi.php');
  $query = mysql_query("select * from tb_guru where kelas='xmm1'");
  
  $no = 1;
  while ($data = mysql_fetch_array($query)) {
  ?>
      <tr>
          <td class="center aligned"><?php echo $no; ?></td>
          <td class="center aligned"><?php echo $data['jam_ke']; ?></td>
          <td class="center aligned"><?php echo $data['nama_guru']; ?></td>
          <td class="center aligned"><?php echo $data['alasan']; ?></td>
          <td class="center aligned"><?php echo $data['penugasan']; ?></td>
          <td class="center aligned"><?php echo $data['jenis_tugas']; ?></td>
          <td>
            <a href="edit_guru.php?id_guru=<?php echo $data['id_guru']; ?> "onclick="return confirm ('Anda Yakin Ingin Mengubh Record ini? ?')" class="btn-floating waves-effect waves-light black"><i class="material-icons">mode_edit</i></a>
            <a href="delete-guru.php?id_guru=<?php echo $data['id_guru']; ?>" onclick="return confirm ('Anda Yakin Ingin Menghapus Record ini?')" class="btn-floating waves-effect waves-light black">
            <i class="material-icons">delete</i>
            </a>
          </td>
        </tr>
    <?php 
    $no++;
  } 
  ?>
        </table>
    </div>
    <br>
</div>
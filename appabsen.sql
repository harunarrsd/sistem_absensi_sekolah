-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 20 Nov 2016 pada 13.05
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `appabsen`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_rekap`
--

CREATE TABLE IF NOT EXISTS `table_rekap` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kelas` varchar(15) NOT NULL,
  `sakit` varchar(100) NOT NULL,
  `izin` varchar(100) NOT NULL,
  `alfa` varchar(100) NOT NULL,
  `semester` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_user`
--

CREATE TABLE IF NOT EXISTS `table_user` (
  `username` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `hak_akses` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `table_user`
--

INSERT INTO `table_user` (`username`, `nama`, `password`, `hak_akses`) VALUES
('admin', 'harun', 'admin123', 'Admin'),
('xak1', 'Sekertaris X AK 1', 'xak1doang', 'XAK1'),
('xak2', 'Sekertaris X AK 2', 'xak2doang', 'XAK2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_xak1`
--

CREATE TABLE IF NOT EXISTS `table_xak1` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `date` varchar(20) NOT NULL,
  `keterangan` varchar(15) NOT NULL,
  `alasan` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `table_xak1`
--

INSERT INTO `table_xak1` (`id`, `nama`, `date`, `keterangan`, `alasan`) VALUES
(5, 'DEVI MAHARANI', '20/11/16', 'alfa', 'cabut');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_xak2`
--

CREATE TABLE IF NOT EXISTS `table_xak2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `keterangan` varchar(15) NOT NULL,
  `alasan` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

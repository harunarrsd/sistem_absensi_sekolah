<!DOCTYPE html>
<html>
<head>
	<title>Absensi SMK N 1 Depok</title>
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="css/absencss.css"  media="screen,projection"/>

</head>
<body background="img/2.png">
<nav class="green accent-3" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" class="brand-logo">Absensi SMK N 1 Depok</a>
      <ul class="right hide-on-med-and-down">
        <li><a class="waves-effect waves-light" href="logout.php">Logout</a></li>
      </ul>
    </div>
  </nav>

	<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>Absensi SMK N 1 Depok</title>
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="css/absencss.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="css/icon.css"  media="screen,projection"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<body background="img/1.jpg">
	<div class="tengah green-text text-accent-3" style="font-family: Bernard MT Condensed">
		<h3 class="center">Absensi SMK N 1 Depok</h3>
		<div class="white container border">
		<form action="proses-login.php?op=in" method="POST">
		<br>
			<div class="container">
        		<div class="input-field col s12">
        			<i class="material-icons prefix">account_circle</i>
          			<input placeholder="Nama Pengguna" id="username" name="username" type="text" class="validate" required />
        		</div>
        	</div>
        	<div class="container">
        		<div class="input-field col s12">
        			<i class="material-icons prefix">lock</i>
          			<input placeholder="Kata Sandi" id="password" name="password" type="password" class="validate" required />
        		</div>
        	</div>
        	<div class="container center">
        		<button class="btn waves-effect waves-light green accent-3" type="submit" name="login">Login</button>
        	</div>
        	<br>
        </form>
		</div>
	</div>

	<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
</body>
</html>
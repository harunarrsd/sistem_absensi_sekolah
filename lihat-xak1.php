<?php include 'inc/navbar.php' ?>

<div class="container z-depth-2">
	<h4 class="center">Absensi X AK 1</h4>
	<p class="divider"></p>
	<div class="container">
        <table class="striped centered responsive-table">
          <thead class="green accent-3 white-text">
            <tr>
              <th data-field="id">No.</th>
              <th data-field="nama">Nama</th>
              <th data-field="date">Tanggal</th>
              <th data-field="keterangan">Keterangan</th>
              <th data-field="alasan">Alasan</th>
            </tr>
          </thead>

          <?php
    include('koneksi/koneksi.php');
    
    $query = mysql_query("SELECT * FROM table_xak1 ORDER BY id ASC") or die(mysql_error());
    
    if(mysql_num_rows($query) == 0){
      
      echo '<tr><td colspan="6">Tidak ada data!</td></tr>';
      
    }else{
      
      $no = 1;
      while($data = mysql_fetch_assoc($query)){
        
        echo '<tr>';
          echo '<td>'.$no.'</td>';
          echo '<td>'.$data['nama'].'</td>';
          echo '<td>'.$data['date'].'</td>';
          echo '<td>'.$data['keterangan'].'</td>';
          echo '<td>'.$data['alasan'].'</td>';
          echo '</tr>';
        
        $no++;
        
      }
      
    }
    ?>
        </table>
    </div>
    <br>
</div>
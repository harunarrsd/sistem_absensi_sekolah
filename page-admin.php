<?php
session_start();
if (!isset($_SESSION['username'])) {
	die("Anda belum login!");
}
if ($_SESSION['hak_akses']!="Admin") {
	die("Anda bukan Admin!");
}
?>

<?php include 'inc/navbar.php' ?>

<div class="container z-depth-2">
	<h4 class="center">Selamat Datang Admin</h4>
	<p class="divider"></p>
	<div class="container">
  		<a class="btn waves-effect waves-light green accent-3 right" href="page-xmm1.php">SARAN</a>
  		<a class='dropdown-button btn  waves-effect waves-light green accent-3' href='#' data-activates='kelas'>Kelas</a>

  		<ul id='kelas' class='dropdown-content'>
    		<li> <a href="admin-xak1.php"> X AK  1</li></a>
    		<li> <a href="admin-xak2.php"> X AK  2</li></a>
  			<li> <a href="admin-xaph1.php">X APH 1</li></a>
  			<li> <a href="admin-xaph2.php">X APH 2</li></a>
  			<li> <a href="admin-xmm1.php"> X MM  1</li></a>
  			<li> <a href="admin-xmm2.php"> X MM  2</li></a>
  			<li> <a href="admin-xrpl1.php">X RPL 1</li></a>
  			<li> <a href="admin-xrpl2.php">X RPL 2</li></a>
  			<li> <a href="admin-xtkr1.php">X TKR 1</li></a>
  			<li> <a href="admin-tkr2.php"> X TKR 2</li></a>
  			<li> <a href="admin-xtsm1.php">X TSM 1</li></a>
  			<li> <a href="admin-xtsm2.php">X TSM 2</li></a>
  		</ul>
	</div>
	<br>
</div>

<script>
	$('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrain_width: false, // Does not change width of dropdown to that of the activator
      hover: true, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: true, // Displays dropdown below the button
      alignment: 'left' // Displays dropdown with edge aligned to the left of button
    }
  );
</script>
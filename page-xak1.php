<?php
session_start();
if (!isset($_SESSION['username'])) {
	die("Anda belum login!");
}
if ($_SESSION['hak_akses']!="XAK1") {
	die("Anda bukan bukan Sekertaris X AK 1!");
}
include 'inc/navbar.php'
?>

<div class="container z-depth-2">
	<h4 class="center">Selamat Datang X AK 1</h4>
	<p class="divider"></p>
	<div class="center">
		LAPORAN KETIDAKHADIRAN GURU DAN SISWA <br>
		SMK NEGERI 1 DEPOK <br>
		TAHUN DIKLAT 2016/2017
	</div>
	<br>
	<div class="container">
		<div class="row">
			<form action="proses-xak1.php" method="post">
			<div class="row">
              <label class="black-text">Nama</label>
              <div class="input-field col s12">
                <select class="browser-default" name="nama" required>
                  <option value="">- Pilih -</option>
                  <option>ADINDA DWI ZULKARNAEN</option>
            	  <option>ALIA NUR FADILLAH</option>
            	  <option>AMANDA FEBRIANTY </option>
            	  <option>ANIS KHOIRUNISA</option>
            	  <option>ANITA NURFAIDZIN</option>
            	  <option>ANNISA SORAYA</option>
            	  <option>ARIFAH HILAWANTI PUTRI</option>
            	  <option>ARNETA DELA AMELIA</option>
            	  <option>BRILLY HALOHO</option>
            	  <option>CHAIRUL MU`MIN</option>
            	  <option>CHANDRA NAZRILLAH</option>
            	  <option>CHIKA RATU DAJU</option>
            	  <option>DENISYA PUTRI</option>
            	  <option>DEVI MAHARANI </option>
            	  <option>DILALAH NURILLAELI</option>
            	  <option>FADILLAH RIZKI AMALIA</option>
            	  <option>FARIDA AISYAH</option>
            	  <option>FATHIYA AULIA AZHIMA</option>
            	  <option>FEBRIYANTI </option>
            	  <option>IRA HERAWATI </option>
            	  <option>LIDIA CLAUDIA</option>
            	  <option>MARINA WAHYUNI SIJABAT</option>
            	  <option>MARSYA ANGELICA BRAMI</option>
            	  <option>METANIA</option>
            	  <option>MICHAEL DESPAMA</option>
            	  <option>MIKO DONRI MARTUA SIMANJUNTAK</option>
            	  <option>MUHAMAD RIZKI</option>
            	  <option>NOVANI TRI SLANI</option>
            	  <option>NURAENI SAFARINA</option>
            	  <option>NURHANA SRI SULASNA PUTRI</option>
            	  <option>NURSIPA PAIJIAH</option>
            	  <option>NURUL AINI</option>
            	  <option>PUTRI RAHMAH HANIFAH</option>
            	  <option>RATIH SULASTRI</option>
            	  <option>RENDY RIANSYAH HIDAYAT</option>
            	  <option>RETNO DWI FATMA</option>
            	  <option>RIFKA AULIA KRISTANTI</option>
            	  <option>ROSDIANA SAFITRI</option>
            	  <option>SABIRA ASHFAQ</option>
            	  <option>SELSA AZZAHRA</option>
            	  <option>SHELVIA OKTAVIANI</option>
            	  <option>SINTA</option>
            	  <option>SYIFA ALICIA PUTRI</option>
            	  <option>TANTI INDRAWATI</option>
            	  <option>TARISHA AMELIA PUSPA RINZIANI</option>
                </select>
              </div>
        	</div>
        	<div class="row">
              <label class="black-text">Keterangan</label>
              <div class="input-field col s12">
                <select class="browser-default" name="ket" required>
                  <option value="">- Pilih -</option>
                  <option>sakit</option>
                  <option>izin</option>
                  <option>alfa</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <textarea class="materialize-textarea" name="alasan" required></textarea>
                <label class="black-text">Alasan</label>
              </div>
            </div>
            <div>
              <a class="btn waves-effect waves-light green accent-3 right" href="lihat-xak1.php"><i class="material-icons right">assignment</i>lihat absensi</a>
              <button class="btn waves-effect waves-light green accent-3 left" type="submit" name="kirim">kirim
                <i class="material-icons right">send</i>
              </button>
            </div>
        	</form>
        </div>
        <br>
	</div>
</div>
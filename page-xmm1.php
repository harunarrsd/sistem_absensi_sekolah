<?php
session_start();
if (!isset($_SESSION['username'])) {
	die("Anda belum login!");
}
if ($_SESSION['hak_akses']!="XMM1") {
	die("Anda bukan Sekertaris X MM 1!");
}
include 'inc/navbar.php'
?>

<?php
if(isset($_POST['kirim'])){
  
  include('koneksi/koneksi.php');
  
  $nama   = $_POST['nama'];
  $datetime=date("d/m/y");
  $ket    = $_POST['ket'];
  $alasan   = $_POST['alasan'];
  
  $input = mysql_query("INSERT INTO table_xmm1 VALUES(NULL, '$nama', '$datetime', '$ket', '$alasan')") or die(mysql_error());

  if($input){
    
    ?>
    <script type="text/javascript">
      alert("Data Berhasil ditambah!");
      window.location.href="page-xmm1.php";
    </script>
    <?php
    
  }else{
    
    ?>
    <script type="text/javascript">
      alert("Data Gagal ditambah!");
      window.location.href="page-xmm1.php";
    </script>
    <?php   
  }

}
?>

<div class="container z-depth-2">
	<h4 class="center">Selamat Datang X MM 1</h4>
	<p class="divider"></p>
	<div class="center">
		LAPORAN KETIDAKHADIRAN SISWA <br>
		SMK NEGERI 1 DEPOK <br>
		TAHUN DIKLAT 2016/2017
	</div>
	<br>
	<div class="container">
		<div class="row">
			<form method="post">
			<div class="row">
              <label class="black-text">Nama</label>
              <div class="input-field col s12">
                <select class="browser-default" name="nama" required>
                  <option value="">- Pilih -</option>
                  <option>ADINDA AULIA PUTRI</option>
                  <option>ADINDA SHALSA NABILA PUTERI</option>
                  <option>ALDY DAFFA ARDIANSYAH</option>
                  <option>ANDINI AULIA SALSABILA</option>
                  <option>BACHTIAR RESTA MUNAF ENDRAPUTRA</option>
                  <option>BAGAS HAN SSADEWA</option>
                  <option>DESNIA DWI RIANGHATI</option>
                  <option>DEVILIA APRILIANI SYANJAYA</option>
                  <option>DWI ANANDA SAEFULHAQ</option>
                  <option>FAHMI KHALIS</option>
                  <option>FARHAN AULIA FIKRI</option>
                  <option>FAUZAN IHZA FAJAR</option>
                  <option>GREGORIUS</option>
                  <option>ILHAM ROBI HARTONO</option>
                  <option>INTAN FANDINI</option>
                  <option>ISNA MARITHARIANI SUBEKTI</option>
                  <option>JONATHAN FERDY IROOTH</option>
                  <option>LARAS DWI CAHYANI</option>
                  <option>MARSHA AFRA FIRYAL</option>
                  <option>MARYAM ARVARAYA WIDAYANTO</option>
                  <option>MAS GILANG RAMADHAN DARMAWAN</option>
                  <option>MAYLINDA KURNIASIH</option>
                  <option>MILA KARMILA</option>
                  <option>MUHAMAD SHAUL FAWZI</option>
                  <option>MUHAMMAD ALDI YANTO</option>
                  <option>MUHAMMAD ARYA RAMADHAN</option>
                  <option>MUHAMMAD NADHIF RIZQULLAH</option>
                  <option>MUHAMMAD PANJI ERDIANSYAH</option>
                  <option>MULYADI</option>
                  <option>MUTHIAH AZZAHRA</option>
                  <option>NABILAH ANJAR RATRI WAHYUDI</option>
                  <option>NABILLA RAFA</option>
                  <option>NADILLA NAZWA</option>
                  <option>NAZLADINA ADITYA</option>
                  <option>NOPI LISNAWATI</option>
                  <option>NUR SAFITRI</option>
                  <option>NURMALASARI</option>
                  <option>NURUL IZZAH ISNAINI</option>
                  <option>OSAMA BIN RICKA</option>
                  <option>RACHA PUTRA NURFADHILAH</option>
                  <option>RAIHAN TEISAR</option>
                  <option>REZA PALUPI ALHAQ</option>
                  <option>SUKMA PERTIWI</option>
                  <option>TRI SILFIA ANGGRAENI</option>
                  <option>WULANDARI</option>
                  <option>ZELIKA PUTRI APRILIA</option>
                  <option>ZILDJIAN ARDHYA PRADIPTA</option>
                </select>
              </div>
        	</div>
        	<div class="row">
              <label class="black-text">Keterangan</label>
              <div class="input-field col s12">
                <select class="browser-default" name="ket" required>
                  <option value="">- Pilih -</option>
                  <option>sakit</option>
                  <option>izin</option>
                  <option>alfa</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <textarea class="materialize-textarea" name="alasan" required></textarea>
                <label class="black-text">Alasan</label>
              </div>
            </div>
            <div>
              <a class="btn waves-effect waves-light green accent-3 right" href="lihat-xmm1.php"><i class="material-icons right">assignment</i>lihat absensi</a>
              <button class="btn waves-effect waves-light green accent-3 left" type="submit" name="kirim">kirim
                <i class="material-icons right">send</i>   
              </button>
            </div>
        	</form>
        </div>
        <br>
	</div>
</div>

<div class="container z-depth-2">
  <p class="divider"></p>
  <div class="center">
    LAPORAN KETIDAKHADIRAN GURU<br>
    SMK NEGERI 1 DEPOK <br>
    TAHUN DIKLAT 2016/2017
  </div>
  <br>
  <div class="container">
    <div class="row">
      <form action="insert-guru.php" method="post">
      <input type="hidden" name="kelas" value="xmm1">
       <div class="row">
              <div class="input-field col s12">
                <textarea class="materialize-textarea" name="jam_ke" required></textarea>
                <label class="black-text">Jam Ke</label>
              </div>
            </div>
       <div class="row">
              <div class="input-field col s12">
                <textarea class="materialize-textarea" name="nama_guru" required></textarea>
                <label class="black-text">NAMA GURU & MATA DIKLAT</label>
              </div>
            </div>     
      <div class="row">
              <label class="black-text">Alasan tidak hadir</label>
              <div class="input-field col s12">
                <select class="browser-default" name="alasan" required>
                  <option value="">- Pilih -</option>
                  <option>sakit</option>
                  <option>izin</option>
                  <option>alfa</option>
                </select>
              </div>
            </div>
      <div class="row">
              <label class="black-text">Penugasan</label>
              <div class="input-field col s12">
                <select class="browser-default" name="penugasan">
                  <option value="">- Pilih -</option>
                  <option>Ada</option>
                  <option>Tidak</option>
                </select>
              </div>
            </div>
      <div class="row">
              <div class="input-field col s12">
                <textarea class="materialize-textarea" name="jenis_tugas" ></textarea>
                <label class="black-text">Jenis Tugas</label>
              </div>
            </div>
            <div>
              <a class="btn waves-effect waves-light green accent-3 right" href="lihat-xmm1.php"><i class="material-icons right">assignment</i>lihat absensi</a>
              <button class="btn waves-effect waves-light green accent-3 left" type="submit">kirim
                <i class="material-icons right">send</i>   
              </button>
            </div>
          </form>
        </div>
        <br>
  </div>
</div>
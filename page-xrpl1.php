<?php
session_start();
if (!isset($_SESSION['username'])) {
	die("Anda belum login!");
}
if ($_SESSION['hak_akses']!="XRPL1") {
	die("Anda bukan Sekertaris X RPL 1!");
}
include 'inc/navbar.php'
?>

<?php
if(isset($_POST['kirim'])){
  
  include('koneksi/koneksi.php');
  
  $nama   = $_POST['nama'];
  $datetime=date("d/m/y");
  $ket    = $_POST['ket'];
  $alasan   = $_POST['alasan'];
  
  $input = mysql_query("INSERT INTO table_xrpl1 VALUES(NULL, '$nama', '$datetime', '$ket', '$alasan')") or die(mysql_error());

  if($input){
    
    ?>
    <script type="text/javascript">
      alert("Data Berhasil ditambah!");
      window.location.href="page-xrpl1.php";
    </script>
    <?php
    
  }else{
    
    ?>
    <script type="text/javascript">
      alert("Data Gagal ditambah!");
      window.location.href="page-xrpl1.php";
    </script>
    <?php   
  }

}
?>

<div class="container z-depth-2">
	<h4 class="center">Selamat Datang X RPL 1</h4>
	<p class="divider"></p>
	<div class="center">
		LAPORAN KETIDAKHADIRAN GURU DAN SISWA <br>
		SMK NEGERI 1 DEPOK <br>
		TAHUN DIKLAT 2016/2017
	</div>
	<br>
	<div class="container">
		<div class="row">
			<form method="post">
			<div class="row">
              <label class="black-text">Nama</label>
              <div class="input-field col s12">
                <select class="browser-default" name="nama" required>
                  <option value="">- Pilih -</option>
                  <option>ADITIA</option>
                  <option>AHMAD MALIK FAJAR SUTISNA</option>
                  <option>AKBAR LUTFIANSYAH</option>
                  <option>AKIRA OKA PRANATA</option>
                  <option>ANANG WICAKSONO</option>
                  <option>ANNA FIROHMATILLAH</option>
                  <option>ANNISA NUR APRILIA </option>
                  <option>ARRYA EKA DESU</option>
                  <option>BUNGA AHMAD </option>
                  <option>CHARLY OKTAVIANUS SIANIPAR</option>
                  <option>DEWA SATRIANI ISKANDAR PUTRA</option>
                  <option>DWI DEVI ROSANDI</option>
                  <option>FARHAN CHANDRA OKTAVIANO</option>
                  <option>FATHURRAHMAN</option>
                  <option>FENDY SETIAWAN</option>
                  <option>FENIKS ATTAR FINURULLAH</option>
                  <option>GILANG SAPUTRO</option>
                  <option>GUNAWAN SANTOSO</option>
                  <option>HENDRA</option>
                  <option>HERZAKY ITSAR AL FATIH</option>
                  <option>IKA NURAINI </option>
                  <option>INDAH SARI</option>
                  <option>KEVIN SENJA SUWONDO</option>
                  <option>KIRANA ZALFAA KUSUMA</option>
                  <option>LUTHFI ARZAKI</option>
                  <option>MOHAMAD HOIRUL UMAM</option>
                  <option>MUHAMMAD AULIA RAHMAN RIDHO</option>
                  <option>MUHAMMAD DICKY APRIZAL </option>
                  <option>MUHAMMAD FACHRUDIN THOHARI</option>
                  <option>MUHAMMAD HANIF</option>
                  <option>MUHAMMAD HARZAH APRIANTO </option>
                  <option>MUHAMMAD OWEN ATHALASEAN</option>
                  <option>MUHAMMAD RAFLI ALPHARES</option>
                  <option>MUHAMMAD RICKY ALPIAN</option>
                  <option>NATASYA ARINI PURWATI</option>
                  <option>RACHMAT ISKANDAR</option>
                  <option>RAFIF MULIA RESWARA</option>
                  <option>RAHMAH AZZAHRA</option>
                  <option>REZA WIDYANTO</option>
                  <option>RIAN BAYU ANANDA</option>
                  <option>RIFAL AGUSTIAWAN</option>
                  <option>RIZALDI MAULA</option>
                  <option>SADAM HERMAWAN</option>
                  <option>SANDO PUTRA WARDANA</option>
                  <option>SHINTIA MUTIARANI</option>
                  <option>WILY YAHYA SRI HARTATA</option>
                  <option>YOGIE</option>
                  <option>YUSUF RIJAL ROBBANI</option>
                </select>
              </div>
        	</div>
        	<div class="row">
              <label class="black-text">Keterangan</label>
              <div class="input-field col s12">
                <select class="browser-default" name="ket" required>
                  <option value="">- Pilih -</option>
                  <option>sakit</option>
                  <option>izin</option>
                  <option>alfa</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <textarea class="materialize-textarea" name="alasan" required></textarea>
                <label class="black-text">Alasan</label>
              </div>
            </div>
            <div>
              <a class="btn waves-effect waves-light green accent-3 right" href="lihat-xak1.php"><i class="material-icons right">assignment</i>lihat absensi</a>
              <button class="btn waves-effect waves-light green accent-3 left" type="submit" name="kirim">kirim
                <i class="material-icons right">send</i>
              </button>
            </div>
        	</form>
        </div>
        <br>
	</div>
</div>
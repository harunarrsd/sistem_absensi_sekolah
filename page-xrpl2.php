<?php
session_start();
if (!isset($_SESSION['username'])) {
	die("Anda belum login!");
}
if ($_SESSION['hak_akses']!="XRPL2") {
	die("Anda bukan Sekertaris X RPL 2!");
}
include 'inc/navbar.php'
?>

<?php
if(isset($_POST['kirim'])){
  
  include('koneksi/koneksi.php');
  
  $nama   = $_POST['nama'];
  $datetime=date("d/m/y");
  $ket    = $_POST['ket'];
  $alasan   = $_POST['alasan'];
  
  $input = mysql_query("INSERT INTO table_xrpl2 VALUES(NULL, '$nama', '$datetime', '$ket', '$alasan')") or die(mysql_error());

  if($input){
    
    ?>
    <script type="text/javascript">
      alert("Data Berhasil ditambah!");
      window.location.href="page-xrpl2.php";
    </script>
    <?php
    
  }else{
    
    ?>
    <script type="text/javascript">
      alert("Data Gagal ditambah!");
      window.location.href="page-xrpl2.php";
    </script>
    <?php   
  }

}
?>

<div class="container z-depth-2">
	<h4 class="center">Selamat Datang X RPL 2</h4>
	<p class="divider"></p>
	<div class="center">
		LAPORAN KETIDAKHADIRAN GURU DAN SISWA <br>
		SMK NEGERI 1 DEPOK <br>
		TAHUN DIKLAT 2016/2017
	</div>
	<br>
	<div class="container">
		<div class="row">
			<form method="post">
			<div class="row">
              <label class="black-text">Nama</label>
              <div class="input-field col s12">
                <select class="browser-default" name="nama" required>
                  <option value="">- Pilih -</option>
                  <option>ABDUHAKIM HATTA LAKSANA</option>
                  <option>ADELIA SYAIDA</option>
                  <option>ADI SUCIPTO</option>
                  <option>ALIFIO BAGUS PRIAMBODO</option>
                  <option>ALISA ADELIA </option>
                  <option>AMELIA SALSABILAH</option>
                  <option>ANGGRAENI XENA PARADITA</option>
                  <option>ANIDA DWIYANI</option>
                  <option>ARIEF YUNUS</option>
                  <option>AZHAR TRIYADI </option>
                  <option>AZHNA SHALZABILLAH</option>
                  <option>BAGAS WICAKSONO</option>
                  <option>CAHYA DINAR PRASTYO</option>
                  <option>DAVID FERDIANSYAH</option>
                  <option>DIMAS PRASETYO AJI</option>
                  <option>DIMAS TRIO ROYSAPUTRA</option>
                  <option>DITA DWI DAMAYANTI</option>
                  <option>FATHUR RAHMAN RIFQI AZZAMI</option>
                  <option>FAUZI IHZA FAJAR</option>
                  <option>GEMA SATRIA</option>
                  <option>HANDY DEWANGGA</option>
                  <option>HAPIPAH</option>
                  <option>HARRY NUGROHO</option>
                  <option>IKE MONALISA SIMARMATA</option>
                  <option>IZAZ ITSHAAR</option>
                  <option>JAGAD DENTANG SANGGANA</option>
                  <option>KUKUH HADI SASONGKO</option>
                  <option>MUHAMAD FARHAN FAUZAR</option>
                  <option>MUHAMAD RIZKI DWI PUTRA</option>
                  <option>MUHAMMAD ANDRI</option>
                  <option>MUHAMMAD FAIZ</option>
                  <option>MUHAMMAD FAQIH MUSTHOFAIN AKHYAR</option>
                  <option>MUHAMMAD RAFLI</option>
                  <option>NINDI TYA KHARISMA</option>
                  <option>RAKKA RESTU KURNIAWAN</option>
                  <option>REZA FADILAH</option>
                  <option>RIFALDY DIMASCANDRA</option>
                  <option>RIFKI PRAMUDIA EKA PRATAMA</option>
                  <option>RIZKI ADI NUGROHO</option>
                  <option>RONI</option>
                  <option>SAKHAR PRANA AKBAR</option>
                  <option>SANDI ARDIJAYA SOPANDI</option>
                  <option>SANDRIO PUTRADITAMA</option>
                  <option>SATRIO KUNCOROJATI</option>
                  <option>SAWABI IKHSAN YAN KADRI</option>
                  <option>WAHYUDI KURNIAWAN</option>
                  <option>WIDIYANI KURNIA</option>
                </select>
              </div>
        	</div>
        	<div class="row">
              <label class="black-text">Keterangan</label>
              <div class="input-field col s12">
                <select class="browser-default" name="ket" required>
                  <option value="">- Pilih -</option>
                  <option>sakit</option>
                  <option>izin</option>
                  <option>alfa</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <textarea class="materialize-textarea" name="alasan" required></textarea>
                <label class="black-text">Alasan</label>
              </div>
            </div>
            <div>
              <a class="btn waves-effect waves-light green accent-3 right" href="lihat-xak1.php"><i class="material-icons right">assignment</i>lihat absensi</a>
              <button class="btn waves-effect waves-light green accent-3 left" type="submit" name="kirim">kirim
                <i class="material-icons right">send</i>
              </button>
            </div>
        	</form>
        </div>
        <br>
	</div>
</div>